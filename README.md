# BlurredGridMenu

## 项目介绍
本项目是基于开源项目[BlurredGridMenu](https://github.com/gotokatsuya/BlurredGridMenu) 进行harmonyos化的移植和开发的。  
用来展示一个高斯模糊的背景图以及自定义数量的Grid菜单。

#### 项目名称：BlurredGridMenu
#### 所属系列：harmonyos的第三方组件适配移植

#### 功能：一个适用于harmonyos的自定义Grid菜单
#### 项目移植状态：完全移植
#### 调用差异：无
#### 原项目GitHub地址：https://github.com/gotokatsuya/BlurredGridMenu

## 支持功能
- 进入页面时，展示背景图和show按钮
- 点击按钮，会出现模糊的背景图和表格菜单
- 点击菜单，出现点击提示，包括点击的菜单和位置信息


## 安装教程

#### 方案一  
 
```
  //核心引入 
  implementation project(':blurredgridmenu')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
mavenCentral()
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:blurredgridmenu:1.0.0'
```

## 使用说明

#### 代码使用  
MainAbilitySlice，UI页面，用来显示"SHOW MENU"按钮和GridMenu菜单及交互事件。

````java
    private BlurImageView blurImageView;
    private Button showButton;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        blurImageView = (BlurImageView) findComponentById(ResourceTable.Id_blurImageView);
        blurImageView.setBlurFactor(15);
        blurImageView.setBlurImageByRes(ResourceTable.Media_back, getContext());

        showButton = (Button) findComponentById(ResourceTable.Id_show_button);
        showButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtils.e("setClickedListener", "gridMenuFragment:" + gridMenuFragment);
                ... ...
            }

        });
    }

    //添加自定义数据, 用于菜单内容显示:
    public void initData() {
        //添加自定义数据
        menus = new ArrayList<>();
        menus.add(new GridMenu("Home", ResourceTable.Media_home));
        menus.add(new GridMenu("Calendar", ResourceTable.Media_calendar));
        menus.add(new GridMenu("Overview", ResourceTable.Media_overview));
        menus.add(new GridMenu("Groups", ResourceTable.Media_groups));
        menus.add(new GridMenu("Lists", ResourceTable.Media_lists));
        menus.add(new GridMenu("Profile", ResourceTable.Media_profile));
        menus.add(new GridMenu("Timeline", ResourceTable.Media_timeline));
        menus.add(new GridMenu("Setting", ResourceTable.Media_settings));
        gridMenuFragment.setupMenu(menus);

        for (int i = 0; i < menus.size(); i++) {
            Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_grid_menu, null, false);
            Image image = (Image) component.findComponentById(ResourceTable.Id_item_grid_icon);
            Text text = (Text) component.findComponentById(ResourceTable.Id_item_grid_title);
            image.setPixelMap(menus.get(i).icon);
            text.setText(menus.get(i).title);
            tableLayout.addComponent(component);
            component.setWidth(300);

            int finalI = i;
            component.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    new ToastDialog(getContext()).setText("Title: " + menus.get(finalI).title + ", Position: " + finalI).show();
                }
            });
        }
    }

````
自定义BlurImageView类，用来设置基本属性
````java
    //设置背景模糊
    public void setBlurImageByRes(int blurImageRes, Context context) {
        PixelMap blurBitmap = FastBlurUtil.doBlur(getPixelMapByRes(blurImageRes), mBlurFactor, true);
        imageView.setPixelMap(blurBitmap);
        //设置图片恢复原状时间， 注释掉后，图片会一直处于模糊状态。
        if (context != null) {
            context.getUITaskDispatcher().delayDispatch(() -> {
                imageView.setPixelMap(getPixelMapByRes(blurImageRes));
            }, 24 * 60 * 60 * 1000);
        }
    }

    //设置普通图片背景
    public void setOriginImageByRes(int originImageRes) {
        try {
            Resource resource = getContext().getResourceManager().getResource(originImageRes);
            ImageSource imageSource = ImageSource.create(resource, new ImageSource.SourceOptions());
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            PixelMap pixelmap = imageSource.createPixelmap(decodingOptions);
            imageView.setPixelMap(pixelmap);
        } catch (Exception e) {
            Logger.getLogger(BlurImageView.class.getName(), e.getMessage());
        }
    }

````
## 效果展示
<img src="./images/image_1.jpg">
<img src="./images/image_2.jpg">
<img src="./images/image_3.jpg">

## License

MIT License

Copyright (c) 2015 500px Inc.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
