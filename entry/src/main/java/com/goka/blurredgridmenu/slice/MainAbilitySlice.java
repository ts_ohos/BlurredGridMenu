package com.goka.blurredgridmenu.slice;

import com.goka.blurredgridmenu.LogUtils;
import com.goka.blurredgridmenu.ResourceTable;
import com.ohos.blurredgridmenu.BlurImageView;
import com.ohos.blurredgridmenu.GridMenu;
import com.ohos.blurredgridmenu.GridMenuFragment;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    Button showButton;
    StackLayout stackLayout;
    GridMenuFragment gridMenuFragment = new GridMenuFragment();
    FractionScheduler fractionScheduler;
    TableLayout tableLayout;
    List<GridMenu> menus;
    BlurImageView blurImageView;
    BlurImageView defaultImageView;
    Image backgroundImageView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        tableLayout = (TableLayout) findComponentById(ResourceTable.Id_table_layout);

        defaultImageView = (BlurImageView) findComponentById(ResourceTable.Id_default_image);
        blurImageView = (BlurImageView) findComponentById(ResourceTable.Id_blurImageView);
        backgroundImageView = (Image) findComponentById(ResourceTable.Id_background_image);

        defaultImageView.setBlurFactor(0);
        defaultImageView.setOriginImageByRes(ResourceTable.Media_back);

        tableLayout.setVisibility(Component.HIDE);
        blurImageView.setVisibility(Component.HIDE);
        backgroundImageView.setVisibility(Component.HIDE);

        initData();

        fractionScheduler = getFractionManager().startFractionScheduler();
        showButton = (Button) findComponentById(ResourceTable.Id_show_button);
        showButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtils.e("setClickedListener", "gridMenuFragment:" + gridMenuFragment);

//                //加载fragment
//                fractionScheduler.add(ResourceTable.Id_main_frame, gridMenuFragment)
//                        .pushIntoStack(gridMenuFragment.getClass().getSimpleName())
//                        .submit();

//                //跳转页面
//                Intent intent = new Intent();
//                intent.setParam("menus", (Serializable) menus);
//                present(new GridMenuFragmentAbilitySlice(), intent);

                blurImageView.setBlurFactor(15);
                blurImageView.setBlurImageByRes(ResourceTable.Media_back, getContext());

                //控制隐藏显示
                tableLayout.setVisibility(Component.VISIBLE);
                backgroundImageView.setVisibility(Component.VISIBLE);
                blurImageView.setVisibility(Component.VISIBLE);
                showButton.setVisibility(Component.HIDE);

            }

        });
    }

    private FractionManager getFractionManager() {
        Ability ability = getAbility();
        if (ability instanceof FractionAbility) {
            FractionAbility fractionAbility = (FractionAbility) ability;
            return fractionAbility.getFractionManager();
        }
        return null;

    }

    public void initData() {
        //添加自定义数据
        menus = new ArrayList<>();
        menus.add(new GridMenu("Home", ResourceTable.Media_home));
        menus.add(new GridMenu("Calendar", ResourceTable.Media_calendar));
        menus.add(new GridMenu("Overview", ResourceTable.Media_overview));
        menus.add(new GridMenu("Groups", ResourceTable.Media_groups));
        menus.add(new GridMenu("Lists", ResourceTable.Media_lists));
        menus.add(new GridMenu("Profile", ResourceTable.Media_profile));
        menus.add(new GridMenu("Timeline", ResourceTable.Media_timeline));
        menus.add(new GridMenu("Setting", ResourceTable.Media_settings));
        gridMenuFragment.setupMenu(menus);

        for (int i = 0; i < menus.size(); i++) {
            Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_grid_menu, null, false);
            Image image = (Image) component.findComponentById(ResourceTable.Id_item_grid_icon);
            Text text = (Text) component.findComponentById(ResourceTable.Id_item_grid_title);
            image.setPixelMap(menus.get(i).icon);
            text.setText(menus.get(i).title);
            tableLayout.addComponent(component);
            component.setWidth(300);

            int finalI = i;
            component.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    new ToastDialog(getContext()).setText("Title: " + menus.get(finalI).title + ", Position: " + finalI).show();
                }
            });
        }
    }


    @Override
    protected void onBackPressed() {
        LogUtils.e("setClickedListener", "gridMenuFragment:" + gridMenuFragment);
        if (tableLayout.getVisibility() == Component.VISIBLE) {
            tableLayout.setVisibility(Component.HIDE);
            blurImageView.setVisibility(Component.HIDE);
            backgroundImageView.setVisibility(Component.HIDE);

            showButton.setVisibility(Component.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
