package com.ohos.blurredgridmenu;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TableLayout;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

public class GridMenuFragment extends Fraction {
    private List<GridMenu> mMenus = new ArrayList<>();
    TableLayout tableLayout;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fragment_main, container, false);
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        LogUtils.e("GridMenuFragment onStart", "onStart:");
        tableLayout = (TableLayout) getComponent().findComponentById(ResourceTable.Id_table_layout);
        initView();

    }

    public void initView() {
        BlurImageView blurImageView = (BlurImageView) getComponent().findComponentById(ResourceTable.Id_blurImageView);
        int blurFactor = BlurImageView.DEFAULT_BLUR_FACTOR;
        blurImageView.setBlurFactor(5);
        blurImageView.setBlurImageByRes(ResourceTable.Media_back, getComponent().getContext());

//        try {
//            Resource resource = getComponent().getResourceManager().getResource(ResourceTable.Media_back);
//            PixelMapElement element = new PixelMapElement(resource);
//            Glide.with(getComponent().getContext()).load(element).fitCenter().into(blurringView);
//        } catch (IOException | NotExistException e) {
//            e.printStackTrace();
//        }


//        List<GridMenu> menus = new ArrayList<>();
//        menus.add(new GridMenu("Home", ResourceTable.Media_home));
//        menus.add(new GridMenu("Calendar", ResourceTable.Media_calendar));
//        menus.add(new GridMenu("Overview", ResourceTable.Media_overview));
//        menus.add(new GridMenu("Groups", ResourceTable.Media_groups));
//        menus.add(new GridMenu("Lists", ResourceTable.Media_lists));
//        menus.add(new GridMenu("Profile", ResourceTable.Media_profile));
//        menus.add(new GridMenu("Timeline", ResourceTable.Media_timeline));
//        menus.add(new GridMenu("Setting", ResourceTable.Media_settings));
        for (int i = 0; i < mMenus.size(); i++) {
            Component component = LayoutScatter.getInstance(getComponent().getContext()).parse(ResourceTable.Layout_item_grid_menu, null, false);
            Image image = (Image) component.findComponentById(ResourceTable.Id_item_grid_icon);
            Text text = (Text) component.findComponentById(ResourceTable.Id_item_grid_title);
            image.setPixelMap(mMenus.get(i).icon);
            text.setText(mMenus.get(i).title);
            tableLayout.addComponent(component);
            component.setWidth(300);

            int finalI = i;
            component.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    new ToastDialog(getComponent().getContext()).setText("Title: " + mMenus.get(finalI).title + ", Position: " + finalI).show();
                }
            });
        }

    }

    public void setupMenu(List<GridMenu> menus) {
        this.mMenus = menus;
    }
}
