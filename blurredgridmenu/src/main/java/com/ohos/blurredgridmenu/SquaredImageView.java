package com.ohos.blurredgridmenu;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;

public class SquaredImageView extends Image implements Component.EstimateSizeListener {

    public SquaredImageView(Context context) {
        super(context);
    }

    public SquaredImageView(Context context, AttrSet attrs) {
        super(context, attrs, null);
    }

    public SquaredImageView(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, "0");
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        setEstimatedSize(EstimateSpec.getSizeWithMode(widthMeasureSpec, widthMeasureSpec),
                EstimateSpec.getSizeWithMode(heightMeasureSpec, heightMeasureSpec));
        return true;
    }


//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
//    }
//

}