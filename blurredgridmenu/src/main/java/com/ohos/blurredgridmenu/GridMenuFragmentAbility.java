package com.ohos.blurredgridmenu;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class GridMenuFragmentAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GridMenuFragmentAbilitySlice.class.getName());
    }
}
