package com.ohos.blurredgridmenu;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

public class GridMenuFragmentAbilitySlice extends AbilitySlice {
    GridMenuFragment gridMenuFragment = new GridMenuFragment();
    FractionScheduler fractionScheduler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        List<GridMenu> menus = new ArrayList<>();
        menus = intent.getSerializableParam("menus");
        gridMenuFragment.setupMenu(menus);

        fractionScheduler = getFractionManager().startFractionScheduler();
        LogUtils.e("setClickedListener", "gridMenuFragment:" + gridMenuFragment);
        fractionScheduler.add(ResourceTable.Id_main_frame, gridMenuFragment)
                .pushIntoStack(gridMenuFragment.getClass().getSimpleName())
                .submit();
    }

    private FractionManager getFractionManager() {
        Ability ability = getAbility();
        if (ability instanceof FractionAbility) {
            FractionAbility fractionAbility = (FractionAbility) ability;
            return fractionAbility.getFractionManager();
        }
        return null;

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
